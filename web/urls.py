from django.urls import path
from .views import index, UploadView

urlpatterns = [
    path('', index),
    path('/upload', UploadView.as_view(), name="upload")
]
