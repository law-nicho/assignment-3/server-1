from django.conf import settings
from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST

import requests
import secrets
import string

def generateCode():
    N = 7
    return ''.join(secrets.choice(string.ascii_uppercase + string.digits) for i in range(N))

# Create your views here.
def index(request):
    response = {
        'rabbitmq_url' : settings.RABBITMQ_URL,
        'username' : settings.USERNAME,
        'password' : settings.PASSWORD,
        'npm' : settings.NPM,
        'vhost' : settings.VHOST,
        'type' : 'exchange',
    }
    return render(request, 'web.html', response)

class UploadView(APIView):
    def post(self, request):
        code = generateCode()

        response = requests.post(
            settings.SERVER_2_URL,
            headers={
                "X-ROUTING-KEY" : code
            },
            files={
                "file" : request.data.get("file")
            },
        )

        if response.status_code == HTTP_201_CREATED:
            return Response(
                headers={
                    "X-ROUTING-KEY" : code
                },
                status=HTTP_201_CREATED
            )
        else:
            return Response(
                response.json(),
                status=HTTP_400_BAD_REQUEST
            )
